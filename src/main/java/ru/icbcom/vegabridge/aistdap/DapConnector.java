package ru.icbcom.vegabridge.aistdap;

import ru.icbcom.aistdapsdkjava.api.client.Client;
import ru.icbcom.aistdapsdkjava.api.client.Clients;
import ru.icbcom.aistdapsdkjava.api.device.Device;
import ru.icbcom.aistdapsdkjava.api.physicalstructure.PhysicalStructureObject;
import ru.icbcom.aistdapsdkjava.api.physicalstructure.PhysicalStructureObjectList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static ru.icbcom.vegabridge.Main.*;

public class DapConnector {
    private static final long LORA_NETWORK_SERVER_OBJECT_TYPE_ID = 44L;
    private static final long LORA_WAN_DEVICE_OBJECT_TYPE_ID = 45L;
    private static final long DATAKOM_D700_OBJECT_TYPE_ID = 43L;

    public static final Client dapClient = Clients.builder()
            .setBaseUrl(appSett.getDapURL())
            .setLogin(appSett.getDapUsername())
            .setPassword(appSett.getDapPassword())
            .build();

    public static Map<String, Device> devices = new HashMap<>();
    public static Map<String, LWDevice> lwdevices = new HashMap<>();

    public static void pushAllLWDevices() {

        Thread refresh = new Thread(() -> {
            devices.clear();
            lwdevices.clear();

            PhysicalStructureObjectList rootObjects = dapClient.physicalStructure().getAllInRoot();
            List<PhysicalStructureObject> loraWanPhysicalStructureObjects =
                    rootObjects.stream()
                            .filter(physicalStructureObject -> physicalStructureObject.getObjectTypeId() == LORA_NETWORK_SERVER_OBJECT_TYPE_ID)
                            .flatMap(physicalStructureObject -> physicalStructureObject.getDescendants().stream())
                            .filter(physicalStructureObject -> physicalStructureObject.getObjectTypeId() == LORA_WAN_DEVICE_OBJECT_TYPE_ID)
                            .filter(PhysicalStructureObject::hasAttachedDevices)
                            .collect(Collectors.toList());

            for (PhysicalStructureObject loraWanPhysicalStructureObject : loraWanPhysicalStructureObjects) {
                String devEUI = loraWanPhysicalStructureObject.getAttributeValueByName("DevEUI").orElseThrow();
                Device device = loraWanPhysicalStructureObject.getAttachedDevices().iterator().next();

                if (device.getObjectTypeId() == DATAKOM_D700_OBJECT_TYPE_ID)
                    devices.put(devEUI, device);
                    lwdevices.put(devEUI, new LWDevice());
            }

            System.out.println("Refresh all devices");
            log.info("Refresh all devices");
        });
        refresh.start();
    }

    public static Long getDeviceID(String devEUI) {
        Device device = devices.get(devEUI);

        return device.getId();
    }

    public static boolean isContainsDevEUI(String devEUI) {
        return devices.containsKey(devEUI);
    }

    public static boolean isLWContainsDevEUI(String devEUI) {
        return lwdevices.containsKey(devEUI);
    }

    public static String getLWLastData(String devEui) {
        return lwdevices.get(devEui).getLastData();
    }

    public static void setLWLastData(String devEui, String lastData) {
        lwdevices.get(devEui).setLastData(lastData);
    }

    public static Long getLWLastTs(String devEui) {
        return lwdevices.get(devEui).getLastTs();
    }

    public static void setLWLastTs(String devEui, Long lastTs) {
        lwdevices.get(devEui).setLastTs(lastTs);
    }

    public static void main(String[] args) {

    }
}

class LWDevice {
    private String lastData;
    private Long lastTs;

    LWDevice() {
        this.lastData = "";
        this.lastTs = 0L;
    }

    public String getLastData() {
        return lastData;
    }

    public Long getLastTs() {
        return lastTs;
    }

    public void setLastData(String lastData) {
        this.lastData = lastData;
    }

    public void setLastTs(Long lastTs) {
        this.lastTs = lastTs;
    }
}