package ru.icbcom.vegabridge.websocket;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import ru.icbcom.vegabridge.vegapackets.*;

import java.io.IOException;
import java.net.URI;
import java.util.Timer;
import java.util.TimerTask;

import static ru.icbcom.vegabridge.Main.*;
import static ru.icbcom.vegabridge.aistdap.DapConnector.*;
import static ru.icbcom.vegabridge.datakompackets.DatakomParser.writeMeasuredData;

public class VegaConnector extends WebSocketClient {

    private static VegaConnector vegaConnector;
    private static VegaRequestBuilder vegaRequestBuilder = new VegaRequestBuilder();
    private Boolean authorized;

    public static void connectToVega() {
        vegaConnector = new VegaConnector(appSett.getVegaWebsocketURI());

        if (!vegaConnector.isOpen()) {
            vegaConnector.connect();
        }
    }

    private void retryConnect() {
        Timer timer = new Timer();
        int reconnectTime = appSett.getVegaWebsocketReconnectTime() * 1000;
        timer.schedule(new TimerTask() {

            @Override
            public void run() {
                System.out.println(vegaConnector.isClosed() + "\r\n" +
                        vegaConnector.isOpen() + "\r\n" +
                        vegaConnector.isReuseAddr());

                if (!vegaConnector.isOpen() && vegaConnector.isClosed()) {
                    vegaConnector.reconnect();
                    System.out.println("Retry connect to Vega Server");
                    log.error("Retry connect to Vega Server");
                }

                timer.cancel();
            }
        }, 100, reconnectTime);
    }

    public VegaConnector(URI serverUri) {
        super(serverUri);
        authorized = false;
    }

    @Override
    public void onOpen(ServerHandshake handshakedata) {
        System.out.println("Open WebSocket");
        log.info("Open WebSocket");
        pushAllLWDevices();

        String auth_req = vegaRequestBuilder.build("auth_req");
        vegaConnector.send(auth_req);
        log.info("Send command to Vega Server:\r\n" + auth_req);
    }

    @Override
    public void onMessage(String message) {
        System.out.println(message);

        VegaResponses vegaResponses = new VegaResponses();
        VegaResponse vegaResponse = vegaResponses.getVegaResponse(message);

        if ("auth_resp".equals(vegaResponse.getCmd())) {
            AuthResp authResp = vegaResponses.getAuthResp(message);
            authorized = authResp.getStatus();

            log.info("Authorized on Vega Server: " + authorized);
        }

        if (authorized && "rx".equals(vegaResponse.getCmd())) {
            RX rx = vegaResponses.getRX(message);

            if (rx.isUpMessage() && isContainsDevEUI(rx.getDevEui()) && rx.getData() != null) {
                log.info(message);

                System.out.println(rx.getData() + " " + rx.getTs());

                if (isLWContainsDevEUI(rx.getDevEui())) {
                    try {
                        if (getLWLastData(rx.getDevEui()).equals(rx.getData())) {

                            Long time = (rx.getTs() - getLWLastTs(rx.getDevEui())) / 1000;

                            if (time >= 60) {
                                writeMeasuredData(getDeviceID(rx.getDevEui()), rx.getData().toUpperCase());

                                log.info("Receive data from Vega Server.\r\nPayload: " + rx.getData() + "\r\n" +
                                        "DevEUI: " + rx.getDevEui());
                            }

                        } else {
                            writeMeasuredData(getDeviceID(rx.getDevEui()), rx.getData().toUpperCase());

                            log.info("Receive data from Vega Server.\r\nPayload: " + rx.getData() + "\r\n" +
                                    "DevEUI: " + rx.getDevEui());
                        }
                    } catch (NullPointerException e) {
                        log.error(e.getMessage());
                    }

                    setLWLastData(rx.getDevEui(), rx.getData());
                    setLWLastTs(rx.getDevEui(), rx.getTs());

                }
            }
        }

    }

    @Override
    public void onClose(int code, String reason, boolean remote) {
        authorized = false;

        retryConnect();
        System.out.println(reason);
        log.info(reason);
    }

    @Override
    public void onError(Exception e) {
        authorized = false;

        retryConnect();
        System.out.println(e.getMessage());
        log.error(e.getMessage());
    }
}
