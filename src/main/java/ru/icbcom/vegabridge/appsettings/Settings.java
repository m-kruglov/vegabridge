package ru.icbcom.vegabridge.appsettings;

import static ru.icbcom.vegabridge.Main.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;

public class Settings {
    private URI vegaWebsocketURI;
    private String vegaWebsocketHost;
    private Integer vegaWebsocketPort;
    private String vegaUsername;
    private String vegaPasword;
    private Integer vegaWebsocketReconnectTime;

    private String dapURL;
    private String dapHost;
    private Integer dapPort;
    private Boolean dapSSL;
    private String dapUsername;
    private String dapPassword;

    private Properties props = new Properties();

    private String getDapURL(String host, Integer port, Boolean ssl) {

        String result = "http://" + host + ":" + port + "/";

        switch (ssl.toString()) {
            case "false": {
                result = "http://" + host + ":" + port + "/";
                break;
            }
            case "true": {
                result = "https://" + host + ":" + port + "/";
                break;
            }
        }

        return result;
    }

    public Settings() {
        this.vegaWebsocketHost = "127.0.0.1";
        this.vegaWebsocketPort = 8002;

        try {
            this.vegaWebsocketURI = new URI("ws://" + this.vegaWebsocketHost + ":" + this.vegaWebsocketPort + "/");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        this.vegaUsername = "vegabridge";
        this.vegaPasword = "&rVQ3S!(rHe.gdz%";
        this.vegaWebsocketReconnectTime = 10;

        this.dapHost = "127.0.0.1";
        this.dapPort = 8080;
        this.dapSSL = false;
        this.dapURL = getDapURL(this.dapHost, this.dapPort, this.dapSSL);

        this.dapUsername = "Admin";
        this.dapPassword = "Admin";
    }

    public Settings(String properties) {
        FileInputStream fis;

        try {
            DirUtils appPath = new DirUtils();
            fis = new FileInputStream(appPath.getProgramDirectory() + File.separator + properties);

            this.props.load(fis);
            this.vegaWebsocketHost = props.getProperty("vega_host");
            this.vegaWebsocketPort = Integer.parseInt(props.getProperty("vega_port"));
            this.vegaWebsocketURI = new URI("ws://" + this.vegaWebsocketHost + ":" + this.vegaWebsocketPort + "/");
            this.vegaUsername = props.getProperty("vega_user");
            this.vegaPasword = props.getProperty("vega_password");
            this.vegaWebsocketReconnectTime = Integer.parseInt(props.getProperty("vega_reconnect_time"));

            this.dapHost = props.getProperty("dap_host");
            this.dapPort = Integer.parseInt(props.getProperty("dap_port"));
            this.dapSSL = Boolean.parseBoolean(props.getProperty("dap_ssl"));
            this.dapURL = getDapURL(this.dapHost, this.dapPort, this.dapSSL);
            this.dapUsername = props.getProperty("dap_user");
            this.dapPassword = props.getProperty("dap_password");

        } catch (IOException e) {
            System.err.println(e.getMessage());
            log.error(e.getMessage());

        } catch (NumberFormatException e) {
            System.err.println(e.getMessage());
            log.error(e.getMessage());
        } catch (URISyntaxException e) {
            System.err.println(e.getMessage());
            log.error(e.getMessage());
        }
    }

    public String getDapURL() {
        return dapURL;
    }

    public String getDapUsername() {
        return dapUsername;
    }

    public String getDapPassword() {
        return dapPassword;
    }

    public URI getVegaWebsocketURI() {
        return vegaWebsocketURI;
    }

    public String getVegaUsername() {
        return vegaUsername;
    }

    public String getVegaPasword() {
        return vegaPasword;
    }

    public int getVegaWebsocketReconnectTime() {
        return vegaWebsocketReconnectTime;
    }
}

class DirUtils {
    private static String getJarName()
    {
        return new File(DirUtils.class.getProtectionDomain()
                .getCodeSource()
                .getLocation()
                .getPath())
                .getName();
    }

    private static boolean runningFromJAR()
    {
        String jarName = getJarName();
        return jarName.contains(".jar");
    }

    public static String getProgramDirectory()
    {
        if (runningFromJAR())
        {
            return getCurrentJARDirectory();
        } else
        {
            return getCurrentProjectDirectory();
        }
    }

    private static String getCurrentProjectDirectory()
    {
        return new File("").getAbsolutePath();
    }

    private static String getCurrentJARDirectory()
    {
        try
        {
            return new File(DirUtils.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()).getParent();
        } catch (URISyntaxException exception)
        {
            exception.printStackTrace();
        }

        return null;
    }
}
