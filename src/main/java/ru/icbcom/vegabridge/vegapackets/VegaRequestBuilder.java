package ru.icbcom.vegabridge.vegapackets;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

import static ru.icbcom.vegabridge.Main.*;

public class VegaRequestBuilder {
    public static void main(String[] args) {

        VegaRequest vegaRequest = new VegaRequest.Builder().setCmd("tx").build();
        System.out.println(vegaRequest);
    }

    public String build(String cmd) {
        VegaRequest vegaRequest = new VegaRequest.Builder().setCmd(cmd).build();
        return vegaRequest.toString();
    }

}

class VegaRequest {
    private String cmd;
    private transient Gson gson;
    private transient JsonElement jsonElement;
    // auth_req
    private String login;
    private String password;
    // tx
    private Boolean status;
    private String err_string;
    private String devEui;
    private String data;
    private Integer port;
    private Boolean ack;

    public String toString() {
        jsonElement = gson.toJsonTree(this);
        return jsonElement.toString();
    }

    VegaRequest(Builder builder) {
        this.cmd = builder.cmd;
        this.gson = builder.gson;
        this.jsonElement = builder.jsonElement;
        // auth_req
        this.login = builder.login;
        this.password = builder.password;
        // tx
        this.status = builder.status;
        this.err_string = builder.err_string;
        this.devEui = builder.devEui;
        this.data = builder.data;
        this.port = builder.port;
        this.ack = builder.ack;
    }

    static class Builder {
        private String cmd;
        private transient Gson gson;
        private transient JsonElement jsonElement;
        // auth_req
        private String login;
        private String password;
        // tx
        private Boolean status;
        private String err_string;
        private String devEui;
        private String data;
        private Integer port;
        private Boolean ack;

        public Builder(){
            this.gson = new Gson();
        }

        public Builder setCmd(String cmd) {
            this.cmd = cmd;

            switch (cmd) {
                case "auth_req": {
                    this.login = appSett.getVegaUsername();
                    this.password = appSett.getVegaPasword();
                    break;
                }

                case "tx": {
                    break;
                }
            }

            return this;
        }

        public Builder setStatus(boolean status) {
            this.status = status;
            return this;
        }

        public Builder setErr_string(String err_string) {
            this.err_string = err_string;
            return this;
        }

        public Builder setDevEui(String devEui) {
            this.devEui = devEui;
            return this;
        }

        public Builder setData(String data) {
            this.data = data;
            return this;
        }

        public Builder setPort(int port) {
            this.port = port;
            return this;
        }

        public Builder setAck(boolean ack) {
            this.ack = ack;
            return this;
        }

        public VegaRequest build() {
            return new VegaRequest(this);
        }
    }
}
