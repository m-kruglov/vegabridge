package ru.icbcom.vegabridge.vegapackets;

public final class AuthResp extends VegaResponse {
    private Boolean status;
    private String err_string;
    private String token;
    private String device_access;
    private Boolean console_enable;
    private String [] command_list;
    /*
    private RXSettings rx_settings;

    private class RXSettings {
        private Boolean unsolicted;
        private String direction;
        private Boolean withMacCommands;

        public Boolean getUnsolicted() {
            return unsolicted;
        }

        public String getDirection() {
            return direction;
        }

        public Boolean getWithMacCommands() {
            return withMacCommands;
        }
    }
     */
    public Boolean getStatus() {
        return status;
    }

    public String getErr_string() {
        return err_string;
    }

    public String getToken() {
        return token;
    }

    public String getDevice_access() {
        return device_access;
    }

    public Boolean getConsole_enable() {
        return console_enable;
    }

    public String[] getCommand_list() {
        return command_list;
    }
    /*
    public RXSettings getRx_settings() {
        return rx_settings;
    }
     */
}
