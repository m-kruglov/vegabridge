package ru.icbcom.vegabridge.vegapackets;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class RX extends VegaResponse {
    private String devEui;
    private String appEui;
    private Long ts;
    private String gatewayId;
    private Integer ack;
    private Integer fcnt;
    private Integer port;
    private String data;
    private String macData;
    private Integer freq;
    private String dr;
    private Integer rssi;
    private Double snr;
    private String type;
    private String packetStatus;

    public String getDevEui() {
        return devEui;
    }

    public String getAppEui() {
        return appEui;
    }

    public Long getTs() {
        return ts;
    }

    public String getGatewayId() {
        return gatewayId;
    }

    public Integer getAck() {
        return ack;
    }

    public Integer getFcnt() {
        return fcnt;
    }

    public Integer getPort() {
        return port;
    }

    public String getData() {
        return data;
    }

    public String getMacData() {
        return macData;
    }

    public Integer getFreq() {
        return freq;
    }

    public String getDr() {
        return dr;
    }

    public Integer getRssi() {
        return rssi;
    }

    public Double getSnr() {
        return snr;
    }

    public String getType() {
        return type;
    }

    public String getPacketStatus() {
        return packetStatus;
    }

    public Boolean isUpMessage() {
        List<String> message = new ArrayList<>(Arrays.asList(this.type.split("\\+")));

        return message.contains(TypeMessages.UNCONF_UP.getTitle()) ||
                message.contains(TypeMessages.CONF_UP.getTitle());
    }
}

enum TypeMessages {
    UNCONF_UP("UNCONF_UP"),
    CONF_UP("CONF_UP");

    private String title;

    TypeMessages(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}