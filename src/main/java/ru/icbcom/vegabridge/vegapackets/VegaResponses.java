package ru.icbcom.vegabridge.vegapackets;

import com.google.gson.Gson;

public class VegaResponses {
    Gson gson = new Gson();

    public AuthResp getAuthResp(String message) {
        return gson.fromJson(message, AuthResp.class);
    }

    public VegaResponse getVegaResponse(String message) {
        return gson.fromJson(message, VegaResponse.class);
    }

    public RX getRX(String message) {
        return gson.fromJson(message, RX.class);
    }
}

