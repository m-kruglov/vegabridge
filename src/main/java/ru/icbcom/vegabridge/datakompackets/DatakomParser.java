package ru.icbcom.vegabridge.datakompackets;

import ru.icbcom.aistdapsdkjava.api.measureddata.MeasuredData;

import java.util.ArrayList;
import java.util.Arrays;

import static ru.icbcom.vegabridge.aistdap.DapConnector.dapClient;
import static ru.icbcom.vegabridge.datakompackets.LibFunc.*;
import static ru.icbcom.vegabridge.Main.*;

public class DatakomParser {

    public static void writeMeasuredData(Long deviceId, String data) {

        byte buff[] = HexStringToByteArray(data);

        if (isValidCRC8(buff)) {
            Long typeFrame = toUnsignedLong(Arrays.copyOfRange(buff, 0, 1),0);
            Long typeDevice = toUnsignedLong(Arrays.copyOfRange(buff, 1, 2),0);

            if (typeDevice.equals(1L)) {

                System.out.println("Packet " + typeFrame);
                log.info("Receive Packet #" + typeFrame + " from device Datakom D700 with ID: " + deviceId);

                switch (typeFrame.intValue()) {
                    case 1: {
                        Packet1 packet1 = new Packet1(deviceId, buff, 43);
                        packet1.writePacket();

                        break;
                    }
                    case 2: {
                        Packet2 packet2 = new Packet2(deviceId, buff, 43);
                        packet2.writePacket();

                        break;
                    }
                    case 3: {
                        Packet3 packet3 = new Packet3(deviceId, buff, 31);
                        packet3.writePacket();

                        break;
                    }
                    case 4: {
                        Packet4 packet4 = new Packet4(deviceId, buff, 29);
                        packet4.writePacket();

                        break;
                    }
                    case 5: {
                        Packet5 packet5 = new Packet5(deviceId, buff, 39);
                        packet5.writePacket();

                        break;
                    }
                    case 6: {
                        Packet6 packet6 = new Packet6(deviceId, buff, 35);
                        packet6.writePacket();

                        break;
                    }
                    case 7: {
                        Packet7 packet7 = new Packet7(deviceId, buff, 35);
                        packet7.writePacket();

                        break;
                    }
                    case 8: {
                        Packet8 packet8 = new Packet8(deviceId, buff, 35);
                        packet8.writePacket();

                        break;
                    }
                }
            }
        } else {
            log.error("Bad CRC in packet from device with ID: " + deviceId + "\r\nCRC: " +
                    getCRC8(buff) + "\r\nPayload: " + data);
        }
    }

    private static class Packet {
        private Long deviceId;
        private Integer packetLength;
        protected ArrayList<MeasuredData> measureds;

        public Long getDeviceId() {
            return deviceId;
        }

        Packet(Long deviceId, byte[] buff, Integer packetLength) {
            this.deviceId = deviceId;
            this.packetLength = packetLength;
            this.measureds = new ArrayList<>();
        }

        public void writePacket() {
            for (MeasuredData m : measureds) {
                if (m.getValue() != null)
                    dapClient.measuredData().insert(m);
            }
        }

    }

    private final static class Packet1 extends Packet{
        private Double mainsVoltageL1;
        private Double mainsVoltageL2;
        private Double mainsVoltageL3;
        private Double mainsVoltageL1_L2;
        private Double mainsVoltageL2_L3;
        private Double mainsVoltageL3_L1;
        private Double mainsCurrentL1;
        private Double mainsCurrentL2;
        private Double mainsCurrentL3;
        private Double mainsNeutralCurrent;

        Packet1(Long deviceId, byte[] buff, Integer packetLength) {
            super(deviceId, buff, packetLength);

            if (buff.length == packetLength) {
                this.mainsVoltageL1 = toIntegerLW(Arrays.copyOfRange(buff, 2, 6)) * 0.1D;
                this.mainsVoltageL2 = toIntegerLW(Arrays.copyOfRange(buff, 6, 10)) * 0.1D;
                this.mainsVoltageL3 = toIntegerLW(Arrays.copyOfRange(buff, 10, 14)) * 0.1D;
                this.mainsVoltageL1_L2 = toIntegerLW(Arrays.copyOfRange(buff, 14, 18)) * 0.1D;
                this.mainsVoltageL2_L3 = toIntegerLW(Arrays.copyOfRange(buff, 18, 22)) * 0.1D;
                this.mainsVoltageL3_L1 = toIntegerLW(Arrays.copyOfRange(buff, 22, 26)) * 0.1D;
                this.mainsCurrentL1 = toIntegerLW(Arrays.copyOfRange(buff, 26, 30)) * 0.1D;
                this.mainsCurrentL2 = toIntegerLW(Arrays.copyOfRange(buff, 30, 34)) * 0.1D;
                this.mainsCurrentL3 = toIntegerLW(Arrays.copyOfRange(buff, 34, 38)) * 0.1D;
                this.mainsNeutralCurrent = toIntegerLW(Arrays.copyOfRange(buff, 38, 42)) * 0.1D;

                addPacketMeasureds();
            } else {
                log.error("Received Packet #1 from device ID " + deviceId + " with bad length." +
                        " Current length = " + buff.length + ", butt must have length = " + packetLength);
            }

        }

        private void addPacketMeasureds() {
            measureds.clear();
            measureds.add(getMainsVoltageL1());
            measureds.add(getMainsVoltageL2());
            measureds.add(getMainsVoltageL3());
            measureds.add(getMainsVoltageL1_L2());
            measureds.add(getMainsVoltageL2_L3());
            measureds.add(getMainsVoltageL3_L1());
            measureds.add(getMainsCurrentL1());
            measureds.add(getMainsCurrentL2());
            measureds.add(getMainsCurrentL3());
            measureds.add(getMainsNeutralCurrent());
        }

        private MeasuredData getMainsVoltageL1() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(100L)
                    .setDapObjectId(getDeviceId())
                    .setDoubleValue(this.mainsVoltageL1);
        }

        private MeasuredData getMainsVoltageL2() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(101L)
                    .setDapObjectId(getDeviceId())
                    .setDoubleValue(this.mainsVoltageL2);
        }

        private MeasuredData getMainsVoltageL3() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(102L)
                    .setDapObjectId(getDeviceId())
                    .setDoubleValue(this.mainsVoltageL3);
        }

        private MeasuredData getMainsVoltageL1_L2() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(104L)
                    .setDapObjectId(getDeviceId())
                    .setDoubleValue(this.mainsVoltageL1_L2);
        }

        private MeasuredData getMainsVoltageL2_L3() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(105L)
                    .setDapObjectId(getDeviceId())
                    .setDoubleValue(this.mainsVoltageL2_L3);
        }

        private MeasuredData getMainsVoltageL3_L1() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(106L)
                    .setDapObjectId(getDeviceId())
                    .setDoubleValue(this.mainsVoltageL3_L1);
        }

        private MeasuredData getMainsCurrentL1() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(108L)
                    .setDapObjectId(getDeviceId())
                    .setDoubleValue(this.mainsCurrentL1);
        }

        private MeasuredData getMainsCurrentL2() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(109L)
                    .setDapObjectId(getDeviceId())
                    .setDoubleValue(this.mainsCurrentL2);
        }

        private MeasuredData getMainsCurrentL3() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(110L)
                    .setDapObjectId(getDeviceId())
                    .setDoubleValue(this.mainsCurrentL3);
        }

        private MeasuredData getMainsNeutralCurrent() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(128L)
                    .setDapObjectId(getDeviceId())
                    .setDoubleValue(this.mainsNeutralCurrent);
        }
    }

    private final static class Packet2 extends Packet {

        private Double gensetVoltageL1;
        private Double gensetVoltageL2;
        private Double gensetVoltageL3;
        private Double gensetVoltageL1_L2;
        private Double gensetVoltageL2_L3;
        private Double gensetVoltageL3_L1;
        private Double gensetCurrentL1;
        private Double gensetCurrentL2;
        private Double gensetCurrentL3;
        private Double gensetNeutralCurrent;

        Packet2(Long deviceId, byte[] buff, Integer packetLength) {
            super(deviceId, buff, packetLength);

            if (buff.length == packetLength) {
                this.gensetVoltageL1 = toIntegerLW(Arrays.copyOfRange(buff, 2, 6)) * 0.1D;
                this.gensetVoltageL2 = toIntegerLW(Arrays.copyOfRange(buff, 6, 10)) * 0.1D;
                this.gensetVoltageL3 = toIntegerLW(Arrays.copyOfRange(buff, 10, 14)) * 0.1D;
                this.gensetVoltageL1_L2 = toIntegerLW(Arrays.copyOfRange(buff, 14, 18)) * 0.1D;
                this.gensetVoltageL2_L3 = toIntegerLW(Arrays.copyOfRange(buff, 18, 22)) * 0.1D;
                this.gensetVoltageL3_L1 = toIntegerLW(Arrays.copyOfRange(buff, 22, 26)) * 0.1D;
                this.gensetCurrentL1 = toIntegerLW(Arrays.copyOfRange(buff, 26, 30)) * 0.1D;
                this.gensetCurrentL2 = toIntegerLW(Arrays.copyOfRange(buff, 30, 34)) * 0.1D;
                this.gensetCurrentL3 = toIntegerLW(Arrays.copyOfRange(buff, 34, 38)) * 0.1D;
                this.gensetNeutralCurrent = toIntegerLW(Arrays.copyOfRange(buff, 38, 42)) * 0.1D;

                addPacketMeasureds();
            } else {
                log.error("Received Packet #2 from device ID " + deviceId + " with bad length." +
                        " Current length = " + buff.length + ", butt must have length = " + packetLength);
            }
        }

        private void addPacketMeasureds() {
            measureds.clear();
            measureds.add(getGensetVoltageL1());
            measureds.add(getGensetVoltageL2());
            measureds.add(getGensetVoltageL3());
            measureds.add(getGensetVoltageL1_L2());
            measureds.add(getGensetVoltageL2_L3());
            measureds.add(getGensetVoltageL3_L1());
            measureds.add(getGensetCurrentL1());
            measureds.add(getGensetCurrentL2());
            measureds.add(getGensetCurrentL3());
            measureds.add(getGensetNeutralCurrent());
        }

        private MeasuredData getGensetVoltageL1() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(200L)
                    .setDapObjectId(getDeviceId())
                    .setDoubleValue(this.gensetVoltageL1);
        }

        private MeasuredData getGensetVoltageL2() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(201L)
                    .setDapObjectId(getDeviceId())
                    .setDoubleValue(this.gensetVoltageL2);
        }

        private MeasuredData getGensetVoltageL3() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(202L)
                    .setDapObjectId(getDeviceId())
                    .setDoubleValue(this.gensetVoltageL3);
        }

        private MeasuredData getGensetVoltageL1_L2() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(204L)
                    .setDapObjectId(getDeviceId())
                    .setDoubleValue(this.gensetVoltageL1_L2);
        }

        private MeasuredData getGensetVoltageL2_L3() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(205L)
                    .setDapObjectId(getDeviceId())
                    .setDoubleValue(this.gensetVoltageL2_L3);
        }

        private MeasuredData getGensetVoltageL3_L1() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(206L)
                    .setDapObjectId(getDeviceId())
                    .setDoubleValue(this.gensetVoltageL3_L1);
        }

        private MeasuredData getGensetCurrentL1() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(208L)
                    .setDapObjectId(getDeviceId())
                    .setDoubleValue(this.gensetCurrentL1);
        }

        private MeasuredData getGensetCurrentL2() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(209L)
                    .setDapObjectId(getDeviceId())
                    .setDoubleValue(this.gensetCurrentL2);
        }

        private MeasuredData getGensetCurrentL3() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(210L)
                    .setDapObjectId(getDeviceId())
                    .setDoubleValue(this.gensetCurrentL3);
        }

        private MeasuredData getGensetNeutralCurrent() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(228L)
                    .setDapObjectId(getDeviceId())
                    .setDoubleValue(this.gensetNeutralCurrent);
        }
    }

    private final static class Packet3 extends Packet {
        private Double mainsTotalActivePower;
        private Double gensetTotalActivePower;
        private Double mainsTotalReactivePower;
        private Double gensetTotalReactivePower;
        private Double mainsTotalApparentPower;
        private Double gensetTotalApparentPower;
        private Double mainsTotalPowerFactor;
        private Double gensetTotalPowerFactor;

        Packet3(Long deviceId, byte[] buff, Integer packetLength) {
            super(deviceId, buff, packetLength);

            if (buff.length == packetLength) {
                this.mainsTotalActivePower = toIntegerLW(Arrays.copyOfRange(buff, 2, 6)) * 0.1D;
                this.gensetTotalActivePower = toIntegerLW(Arrays.copyOfRange(buff, 6, 10)) * 0.1D;
                this.mainsTotalReactivePower = toIntegerLW(Arrays.copyOfRange(buff, 10, 14)) * 0.1D;
                this.gensetTotalReactivePower = toIntegerLW(Arrays.copyOfRange(buff, 14, 18)) * 0.1D;
                this.mainsTotalApparentPower = toIntegerLW(Arrays.copyOfRange(buff, 18, 22)) * 0.1D;
                this.gensetTotalApparentPower = toIntegerLW(Arrays.copyOfRange(buff, 22, 26)) * 0.1D;
                this.mainsTotalPowerFactor = toUnsignedLong(Arrays.copyOfRange(buff, 26, 28),0) * 0.001D;
                this.gensetTotalPowerFactor = toUnsignedLong(Arrays.copyOfRange(buff, 28, 30),0) * 0.001D;

                addPacketMeasureds();
            } else {
                log.error("Received Packet #3 from device ID " + deviceId + " with bad length." +
                        " Current length = " + buff.length + ", butt must have length = " + packetLength);
            }
        }

        public void addPacketMeasureds() {
            measureds.clear();
            measureds.add(getMainsTotalActivePower());
            measureds.add(getGensetTotalActivePower());
            measureds.add(getMainsTotalReactivePower());
            measureds.add(getGensetTotalReactivePower());
            measureds.add(getMainsTotalApparentPower());
            measureds.add(getGensetTotalApparentPower());
            measureds.add(getMainsTotalPowerFactor());
            measureds.add(getGensetTotalPowerFactor());
        }

        private MeasuredData getMainsTotalActivePower() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(115L)
                    .setDapObjectId(getDeviceId())
                    .setDoubleValue(this.mainsTotalActivePower);
        }

        private MeasuredData getGensetTotalActivePower() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(215L)
                    .setDapObjectId(getDeviceId())
                    .setDoubleValue(this.gensetTotalActivePower);
        }

        private MeasuredData getMainsTotalReactivePower() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(123L)
                    .setDapObjectId(getDeviceId())
                    .setDoubleValue(this.mainsTotalReactivePower);
        }

        private MeasuredData getGensetTotalReactivePower() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(223L)
                    .setDapObjectId(getDeviceId())
                    .setDoubleValue(this.gensetTotalReactivePower);
        }

        private MeasuredData getMainsTotalApparentPower() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(119L)
                    .setDapObjectId(getDeviceId())
                    .setDoubleValue(this.mainsTotalApparentPower);
        }

        private MeasuredData getGensetTotalApparentPower() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(219L)
                    .setDapObjectId(getDeviceId())
                    .setDoubleValue(this.gensetTotalApparentPower);
        }

        private MeasuredData getMainsTotalPowerFactor() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(127L)
                    .setDapObjectId(getDeviceId())
                    .setDoubleValue(this.mainsTotalPowerFactor);
        }

        private MeasuredData getGensetTotalPowerFactor() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(227L)
                    .setDapObjectId(getDeviceId())
                    .setDoubleValue(this.gensetTotalPowerFactor);
        }
    }

    private final static class Packet4 extends Packet {
        private Double mainsFrequency;
        private Double gensetFrequency;
        private Double chargeInputVoltage;
        private Double batteryVoltage;
        private Double oilPressure;
        private Double engineTemp;
        private Double fuelLevel;
        private Double oilTemp;
        private Double canopyTemp;
        private Double ambientTemp;
        private Long engineRpm;
        private Long unitMode;
        private Long unitOperationStatus;

        Packet4(Long deviceId, byte[] buff, Integer packetLength) {
            super(deviceId, buff, packetLength);

            if (buff.length == packetLength) {
                this.mainsFrequency = toUnsignedLong(Arrays.copyOfRange(buff, 2, 4),0) * 0.01D;
                this.gensetFrequency = toUnsignedLong(Arrays.copyOfRange(buff, 4, 6),0) * 0.01D;
                this.chargeInputVoltage = toUnsignedLong(Arrays.copyOfRange(buff, 6, 8),0) * 0.01D;
                this.batteryVoltage = toUnsignedLong(Arrays.copyOfRange(buff, 8, 10),0) * 0.01D;
                this.oilPressure = toUnsignedLong(Arrays.copyOfRange(buff, 10, 12),0) * 0.1D;
                this.engineTemp = toUnsignedLong(Arrays.copyOfRange(buff, 12, 14),0) * 0.1D;
                this.fuelLevel = toUnsignedLong(Arrays.copyOfRange(buff, 14, 16),0) * 0.1D;
                this.oilTemp = toUnsignedLong(Arrays.copyOfRange(buff, 16, 18),0) * 0.1D;
                this.canopyTemp = toUnsignedLong(Arrays.copyOfRange(buff, 18, 20),0) * 0.1D;
                this.ambientTemp = toUnsignedLong(Arrays.copyOfRange(buff, 20, 22),0) * 0.1D;
                this.engineRpm = toUnsignedLong(Arrays.copyOfRange(buff, 22, 24),0);
                this.unitMode = toUnsignedLong(Arrays.copyOfRange(buff, 24, 26),0);
                this.unitOperationStatus = toUnsignedLong(Arrays.copyOfRange(buff, 26, 28),0);

                addPacketMeasureds();
            } else {
                log.error("Received Packet #4 from device ID " + deviceId + " with bad length." +
                        " Current length = " + buff.length + ", butt must have length = " + packetLength);
            }
        }

        public void addPacketMeasureds(){
            measureds.clear();
            measureds.add(getMainsFrequency());
            measureds.add(getGensetFrequency());

            if (getChargeInputVoltage().getValue() != 32767000000000003D)
                measureds.add(getChargeInputVoltage());
            if (getBattaryVoltage().getValue() != 32767000000000003D)
                measureds.add(getBattaryVoltage());
            if (getOilPressure().getValue() != 32767000000000003D)
                measureds.add(getOilPressure());
            if (getEngineTemp().getValue() != 32767000000000003D)
                measureds.add(getEngineTemp());
            if (getFuelLevel().getValue() != 32767000000000003D)
                measureds.add(getFuelLevel());
            if (getOilTemp().getValue() != 32767000000000003D)
                measureds.add(getOilTemp());
            if (getCanopyTemp().getValue() != 32767000000000003D)
                measureds.add(getCanopyTemp());
            if (getAmbientTemp().getValue() != 32767000000000003D)
                measureds.add(getAmbientTemp());

            measureds.add(getEngineRpm());
            measureds.add(getUnitMode());
            measureds.add(getUnitOperationStatus());
        }

        private MeasuredData getMainsFrequency() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(107L)
                    .setDapObjectId(getDeviceId())
                    .setDoubleValue(this.mainsFrequency);
        }

        private MeasuredData getGensetFrequency() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(207L)
                    .setDapObjectId(getDeviceId())
                    .setDoubleValue(this.gensetFrequency);
        }

        private MeasuredData getChargeInputVoltage() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(302L)
                    .setDapObjectId(getDeviceId())
                    .setDoubleValue(this.chargeInputVoltage);
        }

        private MeasuredData getBattaryVoltage() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(301L)
                    .setDapObjectId(getDeviceId())
                    .setDoubleValue(this.batteryVoltage);
        }

        private MeasuredData getOilPressure() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(304L)
                    .setDapObjectId(getDeviceId())
                    .setDoubleValue(this.oilPressure);
        }

        private MeasuredData getEngineTemp() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(303L)
                    .setDapObjectId(getDeviceId())
                    .setDoubleValue(this.engineTemp);
        }

        private MeasuredData getFuelLevel() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(305L)
                    .setDapObjectId(getDeviceId())
                    .setDoubleValue(this.fuelLevel);
        }

        private MeasuredData getOilTemp() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(306L)
                    .setDapObjectId(getDeviceId())
                    .setDoubleValue(this.oilTemp);
        }

        private MeasuredData getCanopyTemp() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(307L)
                    .setDapObjectId(getDeviceId())
                    .setDoubleValue(this.canopyTemp);
        }

        private MeasuredData getAmbientTemp() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(308L)
                    .setDapObjectId(getDeviceId())
                    .setDoubleValue(this.canopyTemp);
        }

        private MeasuredData getEngineRpm() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(300L)
                    .setDapObjectId(getDeviceId())
                    .setLongValue(this.engineRpm);
        }

        private MeasuredData getUnitMode() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(400L)
                    .setDapObjectId(getDeviceId())
                    .setLongValue(this.unitMode);
        }

        private MeasuredData getUnitOperationStatus() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(401L)
                    .setDapObjectId(getDeviceId())
                    .setLongValue(this.unitOperationStatus);
        }
    }

    private final static class Packet5 extends Packet {
        private Long numGensetRuns;
        private Long numGensetCranks;
        private Long numaGensetOnLoad;
        private Double engineHoursRun;
        private Double engineHoursSinceLastService;
        private Double engineDaysSinceLastService;
        private Double gensetTotalActiveEnergy;
        private Double gensetTotalInductiveReactiveEnergy;
        private Double gensetTotalCapacitiveReactiveEnergy;

        Packet5(Long deviceId, byte[] buff, Integer packetLength) {
            super(deviceId, buff, packetLength);

            if (buff.length == packetLength) {
                this.numGensetRuns = toIntegerLW(Arrays.copyOfRange(buff, 2, 6));
                this.numGensetCranks = toIntegerLW(Arrays.copyOfRange(buff, 6, 10));
                this.numaGensetOnLoad = toIntegerLW(Arrays.copyOfRange(buff, 10, 14));
                this.engineHoursRun = toIntegerLW(Arrays.copyOfRange(buff, 14, 18)) * 0.01D;
                this.engineHoursSinceLastService = toIntegerLW(Arrays.copyOfRange(buff, 18, 22)) * 0.01D;
                this.engineDaysSinceLastService = toIntegerLW(Arrays.copyOfRange(buff, 22, 26)) * 0.01D;
                this.gensetTotalActiveEnergy = toIntegerLW(Arrays.copyOfRange(buff, 26, 30)) * 0.1D;
                this.gensetTotalInductiveReactiveEnergy = toIntegerLW(Arrays.copyOfRange(buff, 30, 34)) * 0.1D;
                this.gensetTotalCapacitiveReactiveEnergy = toIntegerLW(Arrays.copyOfRange(buff, 34, 38)) * 0.1D;

                addPacketMeasureds();
            } else {
                log.error("Received Packet #5 from device ID " + deviceId + " with bad length." +
                        " Current length = " + buff.length + ", butt must have length = " + packetLength);
            }
        }

        public void addPacketMeasureds() {
            measureds.clear();
            measureds.add(getNumGensetRuns());
            measureds.add(getNumGensetCranks());
            measureds.add(getNumaGensetOnLoad());
            measureds.add(getEngineHoursRun());
            measureds.add(getEngineHoursSinceLastService());
            measureds.add(getEngineDaysSinceLastService());
            measureds.add(getGensetTotalActiveEnergy());
            measureds.add(getGensetTotalInductiveReactiveEnergy());
            measureds.add(getGensetTotalCapacitiveReactiveEnergy());
        }

        private MeasuredData getNumGensetRuns() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(600L)
                    .setDapObjectId(getDeviceId())
                    .setLongValue(this.numGensetRuns);
        }

        private MeasuredData getNumGensetCranks() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(601L)
                    .setDapObjectId(getDeviceId())
                    .setLongValue(this.numGensetCranks);
        }

        private MeasuredData getNumaGensetOnLoad() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(602L)
                    .setDapObjectId(getDeviceId())
                    .setLongValue(this.numaGensetOnLoad);
        }

        private MeasuredData getEngineHoursRun() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(603L)
                    .setDapObjectId(getDeviceId())
                    .setDoubleValue(this.engineHoursRun);
        }

        private MeasuredData getEngineHoursSinceLastService() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(604L)
                    .setDapObjectId(getDeviceId())
                    .setDoubleValue(this.engineHoursSinceLastService);
        }

        private MeasuredData getEngineDaysSinceLastService() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(605L)
                    .setDapObjectId(getDeviceId())
                    .setDoubleValue(this.engineDaysSinceLastService);
        }

        private MeasuredData getGensetTotalActiveEnergy() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(606L)
                    .setDapObjectId(getDeviceId())
                    .setDoubleValue(this.gensetTotalActiveEnergy);
        }

        private MeasuredData getGensetTotalInductiveReactiveEnergy() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(607L)
                    .setDapObjectId(getDeviceId())
                    .setDoubleValue(this.gensetTotalInductiveReactiveEnergy);
        }

        private MeasuredData getGensetTotalCapacitiveReactiveEnergy() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(608L)
                    .setDapObjectId(getDeviceId())
                    .setDoubleValue(this.gensetTotalCapacitiveReactiveEnergy);
        }
    }

    private final static class Packet6 extends Packet {
        private Long shutdownAlarmBits1;
        private Long shutdownAlarmBits2;
        private Long shutdownAlarmBits3;
        private Long shutdownAlarmBits4;
        private Long shutdownAlarmBits5;
        private Long shutdownAlarmBits6;
        private Long shutdownAlarmBits7;
        private Long shutdownAlarmBits8;

        Packet6(Long deviceId, byte[] buff, Integer packetLength) {
            super(deviceId, buff, packetLength);

            if (buff.length == packetLength) {
                this.shutdownAlarmBits1 = toIntegerLW(Arrays.copyOfRange(buff, 2, 6));
                this.shutdownAlarmBits2 = toIntegerLW(Arrays.copyOfRange(buff, 6, 10));
                this.shutdownAlarmBits3 = toIntegerLW(Arrays.copyOfRange(buff, 10, 14));
                this.shutdownAlarmBits4 = toIntegerLW(Arrays.copyOfRange(buff, 14, 18));
                this.shutdownAlarmBits5 = toIntegerLW(Arrays.copyOfRange(buff, 18, 22));
                this.shutdownAlarmBits6 = toIntegerLW(Arrays.copyOfRange(buff, 22, 26));
                this.shutdownAlarmBits7 = toIntegerLW(Arrays.copyOfRange(buff, 26, 30));
                this.shutdownAlarmBits8 = toIntegerLW(Arrays.copyOfRange(buff, 30, 34));

                addPacketMeasureds();
            } else {
                log.error("Received Packet #6 from device ID " + deviceId + " with bad length." +
                        " Current length = " + buff.length + ", butt must have length = " + packetLength);
            }
        }

        public void addPacketMeasureds() {
            measureds.clear();
            measureds.add(getShutdownAlarmBits1());
            measureds.add(getShutdownAlarmBits2());
            measureds.add(getShutdownAlarmBits3());
            measureds.add(getShutdownAlarmBits4());
            measureds.add(getShutdownAlarmBits5());
            measureds.add(getShutdownAlarmBits6());
            measureds.add(getShutdownAlarmBits7());
            measureds.add(getShutdownAlarmBits8());
        }

        private MeasuredData getShutdownAlarmBits1() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(500L)
                    .setDapObjectId(getDeviceId())
                    .setLongValue(this.shutdownAlarmBits1);
        }

        private MeasuredData getShutdownAlarmBits2() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(501L)
                    .setDapObjectId(getDeviceId())
                    .setLongValue(this.shutdownAlarmBits2);
        }

        private MeasuredData getShutdownAlarmBits3() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(502L)
                    .setDapObjectId(getDeviceId())
                    .setLongValue(this.shutdownAlarmBits3);
        }

        private MeasuredData getShutdownAlarmBits4() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(503L)
                    .setDapObjectId(getDeviceId())
                    .setLongValue(this.shutdownAlarmBits4);
        }

        private MeasuredData getShutdownAlarmBits5() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(504L)
                    .setDapObjectId(getDeviceId())
                    .setLongValue(this.shutdownAlarmBits5);
        }

        private MeasuredData getShutdownAlarmBits6() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(505L)
                    .setDapObjectId(getDeviceId())
                    .setLongValue(this.shutdownAlarmBits6);
        }

        private MeasuredData getShutdownAlarmBits7() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(506L)
                    .setDapObjectId(getDeviceId())
                    .setLongValue(this.shutdownAlarmBits7);
        }

        private MeasuredData getShutdownAlarmBits8() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(507L)
                    .setDapObjectId(getDeviceId())
                    .setLongValue(this.shutdownAlarmBits8);
        }

    }

    private final static class Packet7 extends Packet {
        private Long loaddumpAlarmBits1;
        private Long loaddumpAlarmBits2;
        private Long loaddumpAlarmBits3;
        private Long loaddumpAlarmBits4;
        private Long loaddumpAlarmBits5;
        private Long loaddumpAlarmBits6;
        private Long loaddumpAlarmBits7;
        private Long loaddumpAlarmBits8;

        Packet7(Long deviceId, byte[] buff, Integer packetLength) {
            super(deviceId, buff, packetLength);

            if (buff.length == packetLength) {
                this.loaddumpAlarmBits1 = toIntegerLW(Arrays.copyOfRange(buff, 2, 6));
                this.loaddumpAlarmBits2 = toIntegerLW(Arrays.copyOfRange(buff, 6, 10));
                this.loaddumpAlarmBits3 = toIntegerLW(Arrays.copyOfRange(buff, 10, 14));
                this.loaddumpAlarmBits4 = toIntegerLW(Arrays.copyOfRange(buff, 14, 18));
                this.loaddumpAlarmBits5 = toIntegerLW(Arrays.copyOfRange(buff, 18, 22));
                this.loaddumpAlarmBits6 = toIntegerLW(Arrays.copyOfRange(buff, 22, 26));
                this.loaddumpAlarmBits7 = toIntegerLW(Arrays.copyOfRange(buff, 26, 30));
                this.loaddumpAlarmBits8 = toIntegerLW(Arrays.copyOfRange(buff, 30, 34));

                addPacketMeasureds();
            } else {
                log.error("Received Packet #7 from device ID " + deviceId + " with bad length." +
                        " Current length = " + buff.length + ", butt must have length = " + packetLength);
            }
        }

        public void addPacketMeasureds() {
            measureds.clear();
            measureds.add(getLoaddumpAlarmBits1());
            measureds.add(getLoaddumpAlarmBits2());
            measureds.add(getLoaddumpAlarmBits3());
            measureds.add(getLoaddumpAlarmBits4());
            measureds.add(getLoaddumpAlarmBits5());
            measureds.add(getLoaddumpAlarmBits6());
            measureds.add(getLoaddumpAlarmBits7());
            measureds.add(getLoaddumpAlarmBits8());
        }

        private MeasuredData getLoaddumpAlarmBits1() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(508L)
                    .setDapObjectId(getDeviceId())
                    .setLongValue(this.loaddumpAlarmBits1);
        }

        private MeasuredData getLoaddumpAlarmBits2() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(509L)
                    .setDapObjectId(getDeviceId())
                    .setLongValue(this.loaddumpAlarmBits2);
        }

        private MeasuredData getLoaddumpAlarmBits3() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(510L)
                    .setDapObjectId(getDeviceId())
                    .setLongValue(this.loaddumpAlarmBits3);
        }

        private MeasuredData getLoaddumpAlarmBits4() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(511L)
                    .setDapObjectId(getDeviceId())
                    .setLongValue(this.loaddumpAlarmBits4);
        }

        private MeasuredData getLoaddumpAlarmBits5() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(512L)
                    .setDapObjectId(getDeviceId())
                    .setLongValue(this.loaddumpAlarmBits5);
        }

        private MeasuredData getLoaddumpAlarmBits6() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(513L)
                    .setDapObjectId(getDeviceId())
                    .setLongValue(this.loaddumpAlarmBits6);
        }

        private MeasuredData getLoaddumpAlarmBits7() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(514L)
                    .setDapObjectId(getDeviceId())
                    .setLongValue(this.loaddumpAlarmBits7);
        }

        private MeasuredData getLoaddumpAlarmBits8() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(515L)
                    .setDapObjectId(getDeviceId())
                    .setLongValue(this.loaddumpAlarmBits8);
        }

    }

    private final static class Packet8 extends Packet {
        private Long warningAlarmBits1;
        private Long warningAlarmBits2;
        private Long warningAlarmBits3;
        private Long warningAlarmBits4;
        private Long warningAlarmBits5;
        private Long warningAlarmBits6;
        private Long warningAlarmBits7;
        private Long warningAlarmBits8;

        Packet8(Long deviceId, byte[] buff, Integer packetLength) {
            super(deviceId, buff, packetLength);

            if (buff.length == packetLength) {
                this.warningAlarmBits1 =toIntegerLW(Arrays.copyOfRange(buff, 2, 6));
                this.warningAlarmBits2 = toIntegerLW(Arrays.copyOfRange(buff, 6, 10));
                this.warningAlarmBits3 = toIntegerLW(Arrays.copyOfRange(buff, 10, 14));
                this.warningAlarmBits4 = toIntegerLW(Arrays.copyOfRange(buff, 14, 18));
                this.warningAlarmBits5 = toIntegerLW(Arrays.copyOfRange(buff, 18, 22));
                this.warningAlarmBits6 = toIntegerLW(Arrays.copyOfRange(buff, 22, 26));
                this.warningAlarmBits7 = toIntegerLW(Arrays.copyOfRange(buff, 26, 30));
                this.warningAlarmBits8 = toIntegerLW(Arrays.copyOfRange(buff, 30, 34));

                addPacketMeasureds();
            } else {
                log.error("Received Packet #8 from device ID " + deviceId + " with bad length." +
                        " Current length = " + buff.length + ", butt must have length = " + packetLength);
            }
        }

        public void addPacketMeasureds() {
            measureds.clear();
            measureds.add(getWarningAlarmBits1());
            measureds.add(getWarningAlarmBits2());
            measureds.add(getWarningAlarmBits3());
            measureds.add(getWarningAlarmBits4());
            measureds.add(getWarningAlarmBits5());
            measureds.add(getWarningAlarmBits6());
            measureds.add(getWarningAlarmBits7());
            measureds.add(getWarningAlarmBits8());
        }

        private MeasuredData getWarningAlarmBits1() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(516L)
                    .setDapObjectId(getDeviceId())
                    .setLongValue(this.warningAlarmBits1);
        }

        private MeasuredData getWarningAlarmBits2() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(517L)
                    .setDapObjectId(getDeviceId())
                    .setLongValue(this.warningAlarmBits2);
        }

        private MeasuredData getWarningAlarmBits3() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(518L)
                    .setDapObjectId(getDeviceId())
                    .setLongValue(this.warningAlarmBits3);
        }

        private MeasuredData getWarningAlarmBits4() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(519L)
                    .setDapObjectId(getDeviceId())
                    .setLongValue(this.warningAlarmBits4);
        }

        private MeasuredData getWarningAlarmBits5() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(520L)
                    .setDapObjectId(getDeviceId())
                    .setLongValue(this.warningAlarmBits5);
        }

        private MeasuredData getWarningAlarmBits6() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(521L)
                    .setDapObjectId(getDeviceId())
                    .setLongValue(this.warningAlarmBits6);
        }

        private MeasuredData getWarningAlarmBits7() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(522L)
                    .setDapObjectId(getDeviceId())
                    .setLongValue(this.warningAlarmBits7);
        }

        private MeasuredData getWarningAlarmBits8() {
            return dapClient.instantiate(MeasuredData.class)
                    .setDataSourceId(523L)
                    .setDapObjectId(getDeviceId())
                    .setLongValue(this.warningAlarmBits8);
        }

    }
}
