package ru.icbcom.vegabridge.datakompackets;

public class LibFunc {

    public static Long toUnsignedLong(byte[] bytes, int offset) {
        long ret = 0;

        for (int i = 0; i < 4 && i + offset < bytes.length; i++) {
            ret <<= 8;
            ret |= (long) bytes[i] & 0xFF;
        }

        return ret;
    }

    public static Long toIntegerLW(byte[] bytes) {
        if (bytes.length != 4) {
            throw new IllegalArgumentException("Byte buffer must have length 4");
        }

        byte[] data = new byte[4];
        data[0] = bytes[2];
        data[1] = bytes[3];
        data[2] = bytes[0];
        data[3] = bytes[1];

        return toUnsignedLong(data,0);
    }

    public static byte[] HexStringToByteArray(String s) throws IllegalArgumentException {
        int len = s.length();

        if (len % 2 == 1) {
            throw new IllegalArgumentException("Hex string must have even number of characters");
        }

        byte[] data = new byte[len / 2];

        for (int i = 0; i < len; i += 2) {

            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }

        return data;
    }

    public static byte getCRC8(byte[] buff) {
        byte crc = 0;

        if (buff.length > 2) {
            for (int i = 2; i < buff.length - 1; i++) {
                crc += buff[i];
            }
        }

        return crc;
    }

    public static boolean isValidCRC8(byte[] buff) {
        return buff[buff.length - 1] == getCRC8(buff);
    }
}
