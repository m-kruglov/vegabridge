package ru.icbcom.vegabridge;

import org.apache.log4j.Logger;
import ru.icbcom.vegabridge.appsettings.Settings;

import static ru.icbcom.vegabridge.websocket.VegaConnector.connectToVega;

import java.lang.invoke.MethodHandles;

public class Main {

    public static final Settings appSett = new Settings("settings.properties");
    public static final Logger log = Logger.getLogger(MethodHandles.lookup().lookupClass());

    public static void main(String[] args) {
        String mode = args[0];

        if (mode.equals("start")) {
            //System.out.println("Start main method");
            log.info("Start main method");

            connectToVega();

        } else {
            System.exit(1);
        }
    }
}
